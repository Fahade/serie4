package org.mssa.exo9;

import java.util.function.Predicate;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	    Predicate<String> plus4  = (s)-> s.length() > 4;
	    System.out.println(plus4.test("java2s"));
	    
	    Predicate<String> nonvide  = (s)-> !s.isEmpty();
	    System.out.println(nonvide.test("java2"));
	    
	    Predicate<String> j  = (s)-> s.startsWith("j");
	    System.out.println(j.test("java2"));
	    
	    Predicate<String> egal5  = (s)-> s.length() == 5;
	    System.out.println(egal5.test("java2"));
	    
	    Predicate<String> jetegal5  = j.and(egal5) ;
	    System.out.println(jetegal5.test("java2"));

	}

}
