package org.mssa.exo11;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Person p1 = new Person("James","Harden",25);
		Person p2 = new Person("John","Paul",27);
		Person p3 = new Person("Chris","Paul",35);
		Person p4 = null;
		List <Person> personnes = new ArrayList<Person>();
		personnes.add(p1);
		personnes.add(p2);
		personnes.add(p4);
		personnes.add(p3);

		Comparator<String> longueur = (s1,s2) -> s1.length() - s2.length();
		System.out.println(longueur.compare("java2s","eclipse"));

		

	    Comparator<Person> cmpPersonNom = (pe1,pe2) -> pe1.getLastName().compareTo(pe2.getLastName());
	    System.out.println("Comparaison nom entre p1 et p2 : " + cmpPersonNom.compare(p1,p2));
	    System.out.println("Comparaison nom entre p3 et p2 : " +cmpPersonNom.compare(p3,p2));

	    Comparator<Person> cmpPersonPrenom = (pe1,pe2) -> pe1.getFirstName().compareTo(pe2.getFirstName());
	    System.out.println("Comparaison pr�nom entre p1 et p2 : " + cmpPersonPrenom.compare(p1,p2));
	    
	    Comparator<Person> cmpPerson = cmpPersonNom.thenComparing(cmpPersonPrenom) ;
	    System.out.println("Comparaison nom entre p3 et p2 si diff�rents sinon entre pr�noms : " + cmpPerson.compare(p3, p2));
	    
	    Comparator<Person> cmpPersonR = cmpPersonNom.thenComparing(cmpPersonPrenom.reversed()).reversed() ;
	    System.out.println("Comparaison inverse entre p3 et p2 : " + cmpPersonR.compare(p3, p2));
	    
	    System.out.println("Liste personnes pas tri�e : " + personnes);
	    personnes.sort(Comparator.nullsLast(cmpPerson));
	    System.out.println("Liste personnes tri�e : " + personnes);
	    
	}

}
