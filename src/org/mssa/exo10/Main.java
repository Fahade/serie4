package org.mssa.exo10;

import java.util.function.Function;
import java.util.function.BiFunction;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Function<String,String> maj = (s)->s.toUpperCase();
	    System.out.println(maj.apply("java2s"));

	    Function<String,String> chaine = (s)->(s==null) ? "":s;
	    System.out.println(chaine.apply("java2s"));
	    
	    Function<String,Integer> longueur = (s)->(s==null) ? 0:s.length();
	    System.out.println(longueur.apply("java2s"));
	    
	    Function<String,String> parenthese = (s)->(s==null) ? "()":"("+s+")";
	    System.out.println(parenthese.apply("java2s"));
	    
	    BiFunction<String,String,Integer> bi = (s1,s2)->(!s1.contains(s2)) ? -1:s1.indexOf(s2);
	    System.out.println(bi.apply("java2s","eclipse"));
	    System.out.println(bi.apply("java2s","va"));
	    
	    Function<String,Integer> abcdefghi = (s)->(bi.apply(s, "abcdefghi"));
	    System.out.println(abcdefghi.apply("java2s"));

	}

}
